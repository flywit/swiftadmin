<?php
return [
  'name' => 'cloudfiles',
  'title' => '腾讯阿里云OSS上传插件',
  'intro' => '同时支持腾讯云、阿里云OSS文件上传插件',
  'author' => '官方',
  'website' => 'www.swiftadmin.net',
  'version' => '1.0.0',
  'status' => 0,
  'extends' => [
    'title' => '这里是扩展配置信',
  ],
  'rewrite' => [
  ],
  'area' => [
    0 => '600px',
    1 => '650px',
  ],
  'config' => 0,
  'url' => '/plugin/cloudfiles',
];